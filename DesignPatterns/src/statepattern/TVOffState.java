package statepattern;

public class TVOffState implements State{

	@Override
	public void doAction(Context context) {
		// TODO Auto-generated method stub
		
		context.setState(this);  // change the state of the context to this one.
		
		//turn on the tv.
		System.out.println("Tv is truned off"); 
	}
	
}
