package threadMethods;

public class ThreadSleepMethod {

	public static class ThreadClassOne extends Thread {
		public void run() {
			try {
				for (int i = 0; i < 5; i++) {
					System.out.println("Thread class one printing..." + i);
					Thread.sleep(500);
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
	public static class ThreadClassTwo extends Thread {
		public void run() {
			try {
				for (int i = 0; i < 5; i++) {
					System.out.println("Thread class two printing..." + i);
					Thread.sleep(500);
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ThreadClassOne threadOne = new ThreadClassOne();
		
		ThreadClassTwo threadTwo = new ThreadClassTwo();
		
		threadOne.start();
		threadTwo.start();
		
		System.out.println("The main calling thread!!");
	}

}
