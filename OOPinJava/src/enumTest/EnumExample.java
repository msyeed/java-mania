package enumTest;

class EnumExample{  

public enum Season 
{ WINTER, 
   SPRING, 
   SUMMER, 
   FALL }  
  
   public static void main(String[] args) 
   {  
      printASeason(Season.WINTER);
      //printASeason("Spring");
   }

   private static void printASeason(Season aSeason)
   { 
         System.out.println(aSeason);   
   }
} 

