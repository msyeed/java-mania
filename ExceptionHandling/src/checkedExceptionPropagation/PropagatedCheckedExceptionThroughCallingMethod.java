package checkedExceptionPropagation;

import java.io.IOException;



public class PropagatedCheckedExceptionThroughCallingMethod {

	static public class ExceptionClass {
		public void methodThrowsException() throws IOException{
			throw new IOException ("I am throwing an exception!");
		}
	}
	
	//the main method just propagates the excetion
	//as this is the end point of the program thus the program terminates after throwing the stack trace
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub

		ExceptionClass exC = new ExceptionClass();
		
		//this method call throws an exception
		exC.methodThrowsException();
		
		//this code will not be executed
		System.out.println("Now continuing my normal operation.......");
	}
}
