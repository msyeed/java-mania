import java.util.ArrayList;
import java.util.Iterator;



abstract class Person{
	public String name;
	public Person(String name)
	{
		this.name = name;
	}
	
	abstract String getName();
}

class Rajit extends Person{

	public Rajit(String name) {
		super(name);
	}

	@Override
	String getName() {
		return name;
	}	
}

class Mou extends Person{

	public Mou(String name) {
		super(name);
	}

	@Override
	String getName() {
		return name;
	}	
}

public class AbstractExample {

	public static void main(String[] args) {
	
		ArrayList<Person> persons = new ArrayList<>();
		
		Person p = new Rajit("Rajit"); 
		persons.add(p);
		System.out.println(p.getName());   // runtime polymorphism
		p = new Mou("Mou");
		persons.add(p);
		System.out.println(p.getName());   // runtime polymorphism
		
		//Single storage with multiple object types..
		Iterator<Person> it = persons.iterator();
		while(it.hasNext())
			System.out.println(it.next().getName());
	}

}
