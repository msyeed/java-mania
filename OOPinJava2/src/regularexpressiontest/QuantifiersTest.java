package regularexpressiontest;

import java.util.regex.Pattern;

public class QuantifiersTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// X? X occurs once or not at all
		// only one character can be matched!
		System.out.println(Pattern.matches("[amn]?", "a"));//true (a or m or n comes one time)  
		System.out.println(Pattern.matches("[amn]?", "amn"));//false (more than one character)  
		System.out.println(Pattern.matches("[amn]?", "man"));//false (more than one character and not in order)  
		
		// X+ X occurs once or more times
		// If a string contains only the characters (no matter how many times) present within the pattern  it will return true.
		
		System.out.println(Pattern.matches("[amn]+", "amn"));//true (allow more than one occurrences)
		System.out.println(Pattern.matches("[amn]+", "amnamn"));//true (allow more than one occurrences)
		System.out.println(Pattern.matches("[amn]+", "amnTTamn"));//false (T are not there in the pattern)
		System.out.println(Pattern.matches("[amn]+", "amnnma"));//true (a, m and n occurs multiple times)
		System.out.println(Pattern.matches("[amn]+", "amamma"));//true (a, m occurs multiple times)
		System.out.println(Pattern.matches("[amn]+", ""));//false (at least one matching character should be there)
		
		// X* X occurs zero or more times
		// Only differenc with X+ is that it matches an empty string as shown bellow
		System.out.println(Pattern.matches("[amn]*", ""));//true (empty string matches as it contains 0 occurances)
		
		
		// X{n} X occurs n times only
		System.out.println(Pattern.matches("[amn]{6}", "amnnam"));//true (occurs exactly 6 times)
		
		// X{n,} X occurs n or more times
		// X{y,z} X occurs at least y times but less than z times
	}
}
