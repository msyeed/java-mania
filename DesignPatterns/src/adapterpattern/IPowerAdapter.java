package adapterpattern;

public interface IPowerAdapter {
	public int supply120VPower(VoltTypes supplyVoltage);
}
