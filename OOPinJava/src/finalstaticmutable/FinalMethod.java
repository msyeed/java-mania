package finalstaticmutable;

public class FinalMethod {

	public final void testFinalMethod()
	{
		System.out.println("I am final method. Thus Immutable and cannot be overridden!! BE careful!");
	}
	
}
