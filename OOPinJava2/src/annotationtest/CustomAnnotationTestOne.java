package annotationtest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

//define an annotation with one method and its default value
//annotation only applicable to methods
//annotation available at runtime
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface TestAnnotation{
	int age() default 10;
}


public class CustomAnnotationTestOne {
	//applying annotation on a method
	@TestAnnotation(age = 30)
	public void annotationTestMethodOne()
	{
		System.out.println("General annotated method");
	}
	
		
	public static void main(String[] args) {
		
		CustomAnnotationTestOne annotationClass = new CustomAnnotationTestOne();
		
		try {
			//getting the method by name at runtime
			Method m = annotationClass.getClass().getMethod("annotationTestMethodOne", null);
			//getting the annotation and printing its value.
			TestAnnotation annotation = m.getAnnotation(TestAnnotation.class);
			System.out.println("Annotation age = " + annotation.age());
			
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			
			e.printStackTrace();
		}
	}
}
