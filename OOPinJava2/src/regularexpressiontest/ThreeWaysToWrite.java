package regularexpressiontest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ThreeWaysToWrite {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//1st way
		String tobeMatched = "as";
		
		//create the pattern for regular expression
		Pattern p = Pattern.compile(".s");
		//mathch the pattern with the string
		Matcher m = p.matcher(tobeMatched);
		//get the result
		boolean b = m.matches();
		
		System.out.println("Is match found: " + b);
		
		//2nd way
		String tobeMatched2 = "aas";
		
		boolean b2 = Pattern.compile(".s").matcher(tobeMatched2).matches();
		
		System.out.println("Is match found: " + b2);
		
		
		//3rd way
		
		boolean b3 = Pattern.matches("..s", tobeMatched2);
		
		System.out.println("Is match found: " + b3);
	}

}
