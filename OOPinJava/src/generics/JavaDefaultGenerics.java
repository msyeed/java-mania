package generics;

import java.util.ArrayList;
import java.util.List;

public class JavaDefaultGenerics {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<String> myData = new ArrayList();
		
		myData.add("Rajit");
		myData.add(" is 35 years old!");
		
		for(String data : myData)
		{
			System.out.print(data);
		}

	}

}
