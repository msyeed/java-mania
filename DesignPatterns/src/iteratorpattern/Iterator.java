package iteratorpattern;

public interface Iterator {
	public boolean hasNext();
	public boolean isEmpty();
	public Object next();
}
