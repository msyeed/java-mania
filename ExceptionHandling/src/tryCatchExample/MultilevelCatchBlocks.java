package tryCatchExample;

public class MultilevelCatchBlocks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = 10;
		int y = 0;
	
		int array[] = new int[4];
		
		try{
			int z = x / y; //should be divide by 0 exception. It is an unchecked / runtime exception
			System.out.println("Print result: " + z);
			
			System.out.println("Array value: " + array[5]); // should be an array index out of bound runtime / unchecked exception
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println("Array index out of bound Exception Caught! \n" + e.getMessage());
		}
		catch(ArithmeticException e)
		{
			System.out.println("Arithmetic Exception Caught! \n" + e.getMessage());
		}
		
		catch(Exception e)
		{
			System.out.println("Exception Caught! \n" + e.getMessage());
		}
		
		//now this code will be executed
		System.out.println("After the exception!");
	

	}

}
