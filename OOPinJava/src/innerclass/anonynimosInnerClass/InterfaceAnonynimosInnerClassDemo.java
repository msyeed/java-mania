package innerclass.anonynimosInnerClass;

public class InterfaceAnonynimosInnerClassDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		InterfaceforAnonynimosInnerClass anonymiosClass = new InterfaceforAnonynimosInnerClass() {

			@Override
			public void print() {
				// TODO Auto-generated method stub
				System.out
						.println("This is the implementation of Interface Anonynimos Inner Class.");
			}
		};
		
		anonymiosClass.print();
	}
}
