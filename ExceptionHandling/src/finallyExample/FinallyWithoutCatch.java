package finallyExample;

public class FinallyWithoutCatch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try{
		int x = 10;
		int y = 0;
		
		int z = x / y; //should be divide by 0 exception. It is an unchecked / runtime exception
		System.out.println("Print result: " + z);
		}
		
		finally
		{
		//this code will not be executed
		System.out.println("After the exception!");
		}

	}

}
