package observerpattern;

public class TestObserverPattern {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Subject subject = new Subject();
		
		new ClientOne(subject);
		new ClientTwo(subject);
		
		subject.setState(100);
		subject.notifyAllObservers();
		
		subject.setState(99999);
		subject.notifyAllObservers();
		
	}

}
