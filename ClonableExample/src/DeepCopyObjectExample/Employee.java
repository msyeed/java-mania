package DeepCopyObjectExample;

public class Employee implements Cloneable{
	 
	//two primitive data type
	private int empoyeeId;
	private String employeeName;
	
	//object type 
	private Department department;
	 
	//constructor
	public Employee(int id, String name, Department dept)
	{
		this.empoyeeId = id;
		this.employeeName = name;
		this.department = dept;
	}
	
	
	//overriding the clone method to deep copy the department object.
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Employee cloned = (Employee)super.clone();
	    cloned.setDepartment((Department)cloned.getDepartment().clone());
	    return cloned;
	}
	     
	//Accessor/mutators methods will go there
	public int getEmployeeId()
	{
		return empoyeeId;
	}
	
	public void setDepartment(Department dept)
	{
		this.department = dept;
	}
	
	public Department getDepartment()
	{
		return department;
	}

}
