package checkedExceptionPropagation;

import java.io.IOException;

public class HandleCheckedExceptionInCallingMethod {

	static public class ExceptionClass {
		public void methodThrowsException() throws IOException{
			throw new IOException ("I am throwing an exception!");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ExceptionClass exC = new ExceptionClass();
		
		try{
		exC.methodThrowsException();
		}
		catch(IOException e)
		{
			System.out.println("Following Exception handled here: "+ "\n");
			e.getMessage();
		}
		
		System.out.println("Now continuing my normal operation.......");
		
	}

}
