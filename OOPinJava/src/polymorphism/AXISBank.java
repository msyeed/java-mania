package polymorphism;

public class AXISBank extends Bank{
	public float getInterestRate()
	{
		return (float) 9.8;
	}

	public String toString()
	{
		return AXISBank.class.getName();
	}
}
