package DeepCopyObjectExample;

public class Department implements Cloneable
{
    private int id;
    private String name;
 
    public Department(int id, String name)
    {
        this.id = id;
        this.name = name;
    }
    
    //define clone method for all objects.
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    //Accessor/mutators methods will go there
    public void setName(String name)
    {
    	this.name = name;
    }
    
    public String getName()
    {
    	return name;
    }
}
