package InheritanceVisibilityTest;

public class TestRun {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//its a subclass of Person class
		new Student("Student").printVariables();
		
		//in package class but not a subclass of Person class
		new Teacher().printVariables();
	}

}
