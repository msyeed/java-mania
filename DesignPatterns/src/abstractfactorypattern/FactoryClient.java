package abstractfactorypattern;

import abstractfactorypattern.boxfactoryimp.Circle;
import abstractfactorypattern.colorfactoryimp.Red;
import abstractfactorypattern.factories.BoxFactory;
import abstractfactorypattern.factories.ColorFactory;
import abstractfactorypattern.factories.GroupOfFactories;

public class FactoryClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//contact to the governing board of the companies
		FactoryReference factoryGovtBoard = FactoryReference.getFactoryReference();
		
		//get access to the specific factories through the board permission
		GroupOfFactories boxFactory = factoryGovtBoard.getBoxFactory();
		
		GroupOfFactories colorFactory = factoryGovtBoard.getColorFactory();
		
		//with access to the compaines .. now order box and color of your chise.
		
		Circle c = (Circle) ((BoxFactory)boxFactory).getShape("circle");
		c.draw();
		
		Red r = (Red) ((ColorFactory)colorFactory).getColor("red");
		r.paint();
	}

}
