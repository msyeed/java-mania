package AbstractClassTest;

public class Student extends Person{

	private int salary = 0;
	
	public Student(String name) {
		super(name);
		// TODO Auto-generated constructor stub
		//System.out.println("Class: " + this + "   Name:- " +  super.getName()); // same output as bellow
		//System.out.println("Student Class: ");
	}
	
	public Student()
	{
		//super("No name"); //this will call the constructor in the Person class with the argument.
		this("No name");  // this will call the student constructor of this class defined above
		                  // This is recommended to use if the class has one  
		System.out.println("Student Class (Default constructor): " + this + "   Name:- " +  this.getName());
	}

	

	/**
	 * abstract methods need to be implemented
	 */
	public void setSalary(int salary) {
		// TODO Auto-generated method stub
		this.salary = salary/ 2;
		
	}

	public int getSalary() {
		// TODO Auto-generated method stub
		return this.salary;
	}

}
