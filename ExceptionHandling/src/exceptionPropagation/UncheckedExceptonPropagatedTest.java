package exceptionPropagation;

public class UncheckedExceptonPropagatedTest {

	void m() {
		int data = 50 / 0;   // arithmetic exception should occur
	}

	void n() {
		m();
	}

	void p() {
		try {
			n();
		} catch (Exception e) {
			System.out.println("exception handled in p()");  // caught here in the chain
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		UncheckedExceptonPropagatedTest test = new UncheckedExceptonPropagatedTest();
		test.p();
	}

}
