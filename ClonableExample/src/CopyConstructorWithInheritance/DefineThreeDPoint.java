package CopyConstructorWithInheritance;

public class DefineThreeDPoint extends DefinePoint{

	private int z;
	
	//normal creation of this object
	public DefineThreeDPoint(int x, int y, int z)
	{
		super(x,y);
		this.z = z;
	}
	
	
	//copy constructor
	public DefineThreeDPoint(DefineThreeDPoint anotherPoint) {
		
		//calls the super class copy constructor
		super(anotherPoint);
		
		//copy's its own staff
		this.z = anotherPoint.z;
	}
	
	
	public String toString()
	{
		return "{ " + super.toString() + ", " + z + " }";
	}

}
