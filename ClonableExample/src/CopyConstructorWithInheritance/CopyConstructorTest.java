package CopyConstructorWithInheritance;

public class CopyConstructorTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		DefinePoint pointOne = new DefinePoint(2,4);
		
		DefineThreeDPoint threeDPoint = new DefineThreeDPoint(1,2,3);
		
		DefineThreeDPoint threeDPointClone = new DefineThreeDPoint(threeDPoint);
		
		
		System.out.println("Are both same object: " + (threeDPoint == threeDPointClone)); //check if they are same object instances
		System.out.println("Do both object refer to same class: " + (threeDPoint.getClass() == threeDPointClone.getClass()));
		
		
		
		
	}

}
