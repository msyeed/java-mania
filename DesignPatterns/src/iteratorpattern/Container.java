package iteratorpattern;

public interface Container {
	//returns an instance of the iterator that iterate over this container.
	public Iterator getIterator();
}
