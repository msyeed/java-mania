package collectionstest;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		HashSet<String> names = new HashSet<String>();
		
		names.add("rajit");
		names.add("mou");
		names.add("suha");
		
		names.add("rajit"); //cannot allow duplicate
		
		names.add("Rajit"); //this is not a duplicate (case sensitive).
		
		Iterator it = names.iterator();
		
		while(it.hasNext())
		{
			System.out.println(it.next());
		}
	}

}
