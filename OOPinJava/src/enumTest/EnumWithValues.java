package enumTest;

public class EnumWithValues {

	private enum LoginRequirement {
		LOGOUT_STEALTH_MODE(1, "stealth"), ANOYNIMOS_LOGIN(2, "anon"), USER_ACCOUNT_LOGIN(3, "user account"), NO_LOGIN_REQUIRED(
				0, "no login"); // just used for default return type

		private Integer value;
		private String name;

		private LoginRequirement(Integer value, String name) {
			this.value = value;
			this.name = name;
		}

		public Integer getValue() {
			return value;
		}
		
		public String getName()
		{
			return name;
		}
		
		public void setValue(int x){
			this.value = x;
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Login REq: " + LoginRequirement.ANOYNIMOS_LOGIN.getValue() + " name: " + LoginRequirement.ANOYNIMOS_LOGIN.getName());
		System.out.println("Login REq: " + LoginRequirement.USER_ACCOUNT_LOGIN.getValue() + " name: " + LoginRequirement.USER_ACCOUNT_LOGIN.getName());
		
		LoginRequirement.ANOYNIMOS_LOGIN.setValue(10);
		
		System.out.println("Login REq: " + LoginRequirement.ANOYNIMOS_LOGIN.getValue());
	}

}
