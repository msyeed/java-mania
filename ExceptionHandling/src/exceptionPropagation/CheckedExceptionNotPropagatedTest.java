package exceptionPropagation;

import java.io.IOException;

public class CheckedExceptionNotPropagatedTest {

	void m(){
		//Its a compile time error! Editor should ask to handle it!
		//either hand it here with try-catch
		//or propagate it through the calling chain with throws
		throw new IOException("This is compile time error! Not propagated!");
	}

	void n(){
		m();
	}

	void p() {
		try {
			n();
		} catch (Exception e) {
			System.out.println("exception handled in p()");  // Never reach here as the compile time error occurs
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CheckedExceptionNotPropagatedTest test = new CheckedExceptionNotPropagatedTest();
		test.p();
	}

}
