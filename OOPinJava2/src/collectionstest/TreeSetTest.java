package collectionstest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

public class TreeSetTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TreeSet<String> names = new TreeSet<String>();
		
		names.add("rajit");
		names.add("mou");
		names.add("suha");
		
		names.add("rajit"); //cannot allow duplicate
		
		names.add("Rajit"); //this is not a duplicate (case sensitive).
		names.add("Mou");
		names.add("Suha");
		
		Iterator it = names.iterator();
		
		while(it.hasNext())
		{
			System.out.println(it.next());
		}
	}

}
