package PolymorphismTest;

public class Student extends Person{

	private int studentId;
	
	public Student(String name, int id)
	{
		
		super(name);
		// TODO Auto-generated constructor stub
		this.studentId = id;
		
		System.out.println("Student Class: ");
	}
	
	//default constructor
	public Student()
	{
		//super("No name"); //this will call the constructor in the Person class with the argument.
		this("No name", 0);  // this will call the student constructor of this class defined above
		                  // This is recommended to use if the class has one  
		System.out.println("Student Class (Default constructor): ");
	}
	
	
	public int getStudentId()
	{
		return studentId;
	}
	
	//overriding the base class method to print the student id with the name.
	public String toString()
	{
		return super.toString() + " : " + studentId;
	}
	

}
