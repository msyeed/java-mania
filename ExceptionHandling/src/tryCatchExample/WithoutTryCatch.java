package tryCatchExample;

public class WithoutTryCatch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int x = 10;
		int y = 0;
		
		int z = x / y; //should be divide by 0 exception. It is an unchecked / runtime exception
		System.out.println("Print result: " + z);
		
		//this code will not be executed
		System.out.println("After the exception!");
	}

}
