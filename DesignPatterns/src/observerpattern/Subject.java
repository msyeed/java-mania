package observerpattern;

import java.util.ArrayList;
import java.util.List;

//a class or object that is to be observed by the clients.

public class Subject {

	private int state;
	
	private List<Observer> observerClientList = new ArrayList<Observer>();
	
	public int getState()
	{
		return state;
	}
	
	public void setState(int state)
	{
		this.state = state;
	}
	
	public void addObserver(Observer observer)
	{
		observerClientList.add(observer);
	}
	
	public void notifyAllObservers()
	{
		for(Observer obj : observerClientList)
		{
			obj.getUpdateOnSubject();
		}
	}
}
