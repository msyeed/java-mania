package inheritanceTest;


class Person{
	public int height;
	public int weight;
	protected String color;
	private String age;
	
	public Person(){
		height = 0;
		weight = 0;
		color = "brown";
		age = "30";
	}
	
	public Person(String age)
	{
		this.age = age;
	}
	
	private void setColor(String color)
	{
		this.color = color;
	}
	
	public String getAge()
	{
		return this.age;
	}
	
	protected void setInfo(int height, int weight, String age, String color)
	{
		this.height = height;
		this.weight = weight;
		this.age = age;
		setColor(color);
	}
	
	public void print()
	{
		System.out.println("height: " + height);
		System.out.println("weithg: " + weight);
		System.out.println("color: " + color);
		System.out.println("age: " + age);
	}
	
}

public class InheritanceTestOne extends Person{

	public InheritanceTestOne()
	{
		//super is used to invoke super class constructor and must be the first instructon of sub class constructor
		//super();
		super("40");
	}
	
	public void testAccessRightOfSuperClassAttributes()
	{
		//can access directly public and protected member variables of super class
		System.out.println("height: " + height);
		System.out.println("weithg: " + weight); // alternatively can use super keyword, e.g., super.weight
		System.out.println("color: " + color);
		
		//cant not access private member variables
		//System.out.println("age: " + age);
		
		//can access it with public method
		System.out.println("age: " + getAge());
	}
	
	public void testAccessRightOfSuperClassMethods()
	{
		//can access directly public and protected member variables of super class
		super.setInfo(3,4, "20", "red");   // super keyword is optional here
		String age = getAge();
		
		
		//cant not access private member variables
		//System.out.println("age: " + age);
		
		//can access it with public method
		System.out.println("age: " + getAge());
	}
	
	//can override public or protected methods
	@Override
	protected void setInfo(int height, int weight, String age, String color)
	{
		super.setInfo(height, weight, age, color);  // can call superclass method with super keyword
		
		System.out.println("This is subclass overridden method!");
	}
	
	public static void main(String[] args) {
		Person p = new Person();
		p.setInfo(1, 2, "40", "Black");   // call parent class setInfo
		p.print();
		
		//this is runtime polymorphism
		Person p1 = new InheritanceTestOne();
		p1.setInfo(2, 1, "20", "red");   // call sub class setInfo
		p1.print();
	}

}
