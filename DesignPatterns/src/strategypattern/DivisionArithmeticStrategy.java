package strategypattern;

public class DivisionArithmeticStrategy implements IStrategy{

	@Override
	public Integer doArithmetic(Integer num1, Integer num2) {
		// TODO Auto-generated method stub
		return (num2 == 0) ? null : num1 / num2;
	}

}
