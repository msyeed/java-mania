package InterfaceTest;

/**
 * Enforce methods to be implemented.
 * Have a common interface for the functionality
 * @author admin
 *
 */
public interface Person {
	
	//can have constants in a interface
	final int salaryMultiplier = 2;
	
	//!!!!!cannot have variables
	//int personSalary = 0;
	//String personName = "";
	
	
	//!!!!! interface do not have any constructor
	
	//interface do not have method body.
	public void setName(String name);
	public String getName();
	
	public abstract void setSalary(int salary);
	public abstract int getSalary();
}
