package facadepattern;

public class ShapeBuilder {
	
	private Circle circleObj;
	private Rectangle rectangleObj;
	private Square squareObj;
	
	public ShapeBuilder()
	{
		circleObj = new Circle();
		rectangleObj = new Rectangle();
		squareObj = new Square();		
	}
	
	/**
	 * methods to provide services of the underlying system.�
	 * It should not provide the objects to the client.
	 * In such case the pattern would be FACTORY design pattern.
	 * ----------------------------------------------------------
	 */
	public void drawCirlce()
	{
		circleObj.draw();
	}
	
	public void drawRectangle()
	{
		rectangleObj.draw();
	}
	
	public void drawSquare()
	{
		rectangleObj.draw();
	}

}
