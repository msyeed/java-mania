package annotationtest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

//create an annotation to indicate whether a test will
//be executed or not.
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface Test {
	String info() default "";
}


//a dummy test suit with the annotation
//for every test case.
class MyTestSuit {

	@Test(info = "Test it")
	public void testCaseOne(String message) {
		System.out.println("Test One:- " + message);
	}

	@Test(info = "Don't")
	public void testCaseTwo(String message) {
		System.out.println("Test Two:- " + message);
	}

	@Test(info = "Test it")
	public void testCaseThree(String message) {
		System.out.println("Test Three:- " + message);
	}
}


//annotation parser class to execute test case based on the annotation text
class TestAnnotationParserAndExecutor {
	public void parseAndExecute(Class clazz) throws Exception {
		
		//get all the methods in the given test suit class 
		Method[] methods = clazz.getMethods();

		//for each method..
		for (Method method : methods) {
			//if it has test annotation associated with it then..
			if (method.isAnnotationPresent(Test.class)) {
				//get the annotation text
				Test test = method.getAnnotation(Test.class);
				String info = test.info();
				//if the text ask to execute the test then
				if ("Test it".equals(info)) {
					//invoke the method
					method.invoke(clazz.newInstance(), info);
				}
			}
		}

	}
}

public class CustomAnnotationTestEngine {

	public static void main(String[] args) throws Exception{
		// Get the annotation parser and pass the test suit class to execute it..
		TestAnnotationParserAndExecutor pandE = new TestAnnotationParserAndExecutor();
		pandE.parseAndExecute(MyTestSuit.class);
	}

}
