package interfaceTest;

interface PrintX{
	void printX();
}

interface PrintY{
	void printY();
}

interface PrintMain extends PrintX, PrintY{
	void print();
}

public class NestedInterfaceTest implements PrintMain{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		PrintX print = new NestedInterfaceTest();
	}

	@Override
	public void printX() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void printY() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		
	}

}
