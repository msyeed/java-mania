package sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

//NOTE: Specify the type for the Comparable interface
class Teacher implements Comparable<Teacher>{

	private int age = 0;
	private String name = null;
	
	public Teacher(int age, String name)
	{
		this.age = age;
		this.name = name;
	}
	
	//this method provides the sorting order based on 
	//the age data member of student class
	public int compareTo(Teacher arg0) {
		Teacher st = (Teacher)arg0;
		//define the sorting order
		if(this.age == st.age)
			return 0;
		else if(this.age > st.age)
			return 1;
		else
			return -1;
	}
	
	public String toString()
	{
		return this.name + "    " + this.age;
	}
	
}


public class ComparableInterfaceTest {

	public static void main(String[] args) {
		ArrayList<Teacher> teachers = new ArrayList<Teacher>();
		teachers.add(new Teacher(20, "suha"));
		teachers.add(new Teacher(35, "rajit"));
		teachers.add(new Teacher(32, "mou"));
		
		//now sort it
		Collections.sort(teachers);
		
		Iterator<Teacher> it = teachers.iterator();
		while(it.hasNext())
			System.out.println("Student into: " + it.next().toString());
	}

}
