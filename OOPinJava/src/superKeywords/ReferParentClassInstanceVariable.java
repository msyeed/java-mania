package superKeywords;

class Parent{
	public int a = 10;
	int c = 30;
	protected int d = 40;
	private int b = 20;
	
	int commonVar = 10;
}

class Child extends Parent{
	
	
	public void display()
	{
		//can access private or protected or package variable
		System.out.println(a);  // same as System.out.println(super.a);
		System.out.println(c);  // same as System.out.println(super.b);
		System.out.println(d);  // same as System.out.println(super.c);
		
		//compiler error: cannot access private variables
		//System.out.println(b);
	}
	
	//this will shadow /hide the super class variable
	//you need to use super to distinguish between the two.
	//NOTE: not a recommended practice!!
	int commonVar = 20;
	
	public void dispalyCommonVar()
	{
		System.out.println(super.commonVar); //print super class instance
		System.out.println(commonVar); // print subclass instance
	}
}

public class ReferParentClassInstanceVariable {

	public static void main(String[] args) {
		Child c = new Child();
		c.display();
		c.dispalyCommonVar();
	}

}
