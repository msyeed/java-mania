package sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

class NewTeacher {

	private int age = 0;
	private String name = null;

	public NewTeacher(int age, String name) {
		this.age = age;
		this.name = name;
	}

	public int getAge() {
		return this.age;
	}

	public String getName() {
		return this.name;
	}

	public String toString() {
		return this.name + "    " + this.age;
	}

}

// Sort according to age.
class AgeComparator implements Comparator<NewTeacher> {

	public int compare(NewTeacher arg0, NewTeacher arg1) {
		// Define the sorting order
		if (arg0.getAge() == arg1.getAge())
			return 0;
		else if (arg0.getAge() > arg1.getAge())
			return 1;
		else
			return -1;
	}

}

// Sort according to name.
class NameComparator implements Comparator<NewTeacher> {

	public int compare(NewTeacher arg0, NewTeacher arg1) {
		// use compareTo() method of String class for sorting
		return arg0.getName().compareTo(arg1.getName());
	}

}

public class ComparatorInterfaceTest {

	public static void main(String[] args) {
		ArrayList<NewTeacher> teachers = new ArrayList<NewTeacher>();
		teachers.add(new NewTeacher(20, "suha"));
		teachers.add(new NewTeacher(35, "rajit"));
		teachers.add(new NewTeacher(32, "mou"));

		// now sort it
		Collections.sort(teachers, new AgeComparator());

		Iterator<NewTeacher> it = teachers.iterator();
		while (it.hasNext())
			System.out.println("Teacher into: " + it.next().toString());

		// now sort it
		Collections.sort(teachers, new NameComparator());

		Iterator<NewTeacher> its = teachers.iterator();
		while (its.hasNext())
			System.out.println("Teacher into: " + its.next().toString());
	}

}
