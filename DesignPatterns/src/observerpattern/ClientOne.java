package observerpattern;

public class ClientOne extends Observer{

	public ClientOne(Subject subject)
	{
		this.aSubject = subject;
		aSubject.addObserver(this);
	}
	
	public void getUpdateOnSubject(){
		System.out.println("Client One: gets the state update. The state is:- " + aSubject.getState());
	}
}
