package exceptionhandlingWithMethodOverloading;

import java.io.IOException;

class Parent{  
	  void msg() throws Exception
	  {System.out.println("parent");}  
	}

public class SuperSubTestThree  extends Parent{  
	  void msg() //throws Exception
	  {System.out.println("child");}  
	  
	  public static void main(String args[]){  
	   Parent p=new SuperSubTestThree();  
	   try {
		p.msg();
	} catch (Exception e) {
		e.printStackTrace();
	}
	   
	  }
	}  