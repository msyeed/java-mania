package superKeywords;


class Vehicles2 {
	Vehicles2() {
		System.out.println("Vehicles are created");
	}
	
	Vehicles2(int speed) {
		System.out.println("Vehicles are created... with speed " + speed);
	}
}

class Bikes2 extends Vehicles2 {
	int speed;

	//Explicitly call super class overloaded constructor
	//NOTE: must be the first instructon in the constructor.
	Bikes2() {
		super(20);
		this.speed = 20;
		System.out.println("Bikes speed: " + speed);
	}
}


public class InvokeParentClassConstructor3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bikes2 bike = new Bikes2();
	}

}
