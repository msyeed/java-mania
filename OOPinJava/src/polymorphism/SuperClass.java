package polymorphism;

public class SuperClass {
	public String printMsg()
	{
		return "I am super class :)";
	}
	
	public String toString()
	{
		return SuperClass.class.getName();
	}

}
