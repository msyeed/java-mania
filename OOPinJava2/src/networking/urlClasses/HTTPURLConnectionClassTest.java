package networking.urlClasses;

import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPURLConnectionClassTest {

	public static void main(String[] args) {
		try {
			URL url = new URL("http://www.javatpoint.com/java-tutorial");
			HttpURLConnection huc = (HttpURLConnection) url.openConnection();
			for (int i = 1; i <= 8; i++) {
				System.out.println(huc.getHeaderFieldKey(i) + " = "
						+ huc.getHeaderField(i));
			}
			
			System.out.println("\n\nRequest method:- " + huc.getRequestMethod());
			System.out.println("Response code:- " + huc.getResponseCode());
			System.out.println("Read timeout:- " + huc.getReadTimeout());
			System.out.println("Response message:- " + huc.getResponseMessage());
			
			huc.disconnect();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
