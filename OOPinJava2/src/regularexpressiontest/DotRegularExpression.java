package regularexpressiontest;

import java.util.regex.Pattern;

public class DotRegularExpression {

	public static void main(String[] args) {
		// dot represents a single character
		// along with dot the number of characters in the pattern represents the TOTAL LENGTH of the
		// string to be matched (see the third example)
		
		System.out.println(Pattern.matches(".s", "as"));//true (2nd char is s)  
		System.out.println(Pattern.matches(".s", "mk"));//false (2nd char is not s)  
		System.out.println(Pattern.matches(".s", "mst"));//false (has more than 2 char)  
		System.out.println(Pattern.matches(".s", "amms"));//false (has more than 2 char)  
		System.out.println(Pattern.matches("..s", "mas"));//true (3rd char is s)  
	}

}
