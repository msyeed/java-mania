package PolymorphismTest;

public class Teacher extends Person {

	private int teacherSalary;
	
	public Teacher(String name, int teacherSalary) 
	{
		
		super(name);
		// TODO Auto-generated constructor stub
		this.teacherSalary = teacherSalary;
		
		System.out.println("Teachner Class:");
	}

	
	//overriding the base class method to print the teacher salary with the name.
	public String toString()
	{
		return super.toString() + " : " + teacherSalary;
	}
	
}
