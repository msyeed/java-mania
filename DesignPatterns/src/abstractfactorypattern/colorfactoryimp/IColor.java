package abstractfactorypattern.colorfactoryimp;

public interface IColor {
	void paint();
}
