package threadCreation;

public class ThreadTestClass {
	
	
	public static void main(String []args)
	{
		ClassExtendingThreadClass threadClass = new ClassExtendingThreadClass();
		
		//strat the thread
		threadClass.start();
		
		//continue this thread
		System.out.println("Main thread running... print 1");
		
		
		//create a runnable instance
		ClassImplementingRunnable runnableInterface = new ClassImplementingRunnable();
		
		//create a thread to run the runnable
		Thread runThread = new Thread(runnableInterface);
		
		//start the thread
		runThread.start();
		
		//continue this thread..
		System.out.println("Main thread running... print 2");
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		runThread.interrupt();
	}

}
