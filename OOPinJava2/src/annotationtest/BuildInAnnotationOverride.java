package annotationtest;

class ParentClass
{
	public void printSomething()
	{
		System.out.println("I am printing in parent..");
	}
}

public class BuildInAnnotationOverride extends ParentClass{

	@Override
	public void printSomething()
	{
		System.out.println("I am printing in child..");
	}
	

	public void printsomething()
	{
		System.out.println("I want to override but wrong spelled it without any trace :(..");
	}
	
	public static void main(String[] args) {
		new BuildInAnnotationOverride().printSomething();
	}

}
