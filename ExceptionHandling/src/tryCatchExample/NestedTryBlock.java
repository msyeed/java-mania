package tryCatchExample;

public class NestedTryBlock {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

				int x = 10;
				int y = 0;
			
				int array[] = new int[4];
				
				try
				{
					try{
						//should be divide by 0 exception. It is an unchecked / runtime exception
						int z = x / y; 
						System.out.println("Print result: " + z);
					}	
					catch(ArithmeticException e)
					{
						System.out.println("Arithmetic Exception Caught! \n" + e.getMessage());
					}
				
					try
					{
						// should be an array index out of bound runtime / unchecked exception
						System.out.println("Array value: " + array[5]); 
					}
					catch(ArrayIndexOutOfBoundsException e)
					{
						System.out.println("Array index out of bound Exception Caught! \n" + e.getMessage());
					}				
				}
				catch(Exception e)
				{
					System.out.println("Exception Caught! \n" + e.getMessage());
				}
				
				//now this code will be executed
				System.out.println("After the exception!");
	}

}
