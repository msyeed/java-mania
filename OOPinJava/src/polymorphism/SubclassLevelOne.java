package polymorphism;

public class SubclassLevelOne extends SuperClass{

	public String printMsg()
	{
		return "I am sub class level 1:)";
	}
	
	public String toString()
	{
		return SubclassLevelOne.class.getName();
	}
}
