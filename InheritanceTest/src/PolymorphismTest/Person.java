package PolymorphismTest;

public class Person 
{
	private String name;
	
	public Person(String name)
	{
		this.name = name;
		System.out.println("Person Class: Person Name:- " +  this.name);
	}
	
	
	public String toString()
	{
		return name;
	}
	
	public String getName()
	{
		return name;
	}
}
