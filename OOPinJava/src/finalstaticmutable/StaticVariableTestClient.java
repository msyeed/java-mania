package finalstaticmutable;

public class StaticVariableTestClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(StaticVariableTest.name);
		
		//cannot access a private static variable by its name.
		//System.out.println(StaticVariableTest.age);

		//you need setter and getter instead.
		System.out.println(StaticVariableTest.getAge());
		
		
		
		StaticVariableTest testObj = new StaticVariableTest();
		
		//you cannot call an static method with a object reference.
		//because its consistent to all objects.
		testObj.setAge("50");
		testObj.getAge();
	}

}
