package DeepCopyObjectExample;

public class TestCloning {

	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		
		        
        Department hr = new Department(1, "Human Resource");
        Employee original2 = new Employee(1, "Admin", hr);
        Employee cloned2 = (Employee) original2.clone();
 
        //Let change the department name in cloned object and we will verify in original object
        cloned2.getDepartment().setName("Finance");
 
        System.out.println(original2.getDepartment().getName());
        System.out.println(cloned2.getDepartment().getName());

	}

}
