/**
 * An abstract class can have: 
 * Abstract methods (with no method body). 
 * Methods with implementation. 
 * Main method. 
 * Constructor. 
 * All kinds of Data members / Member variables (final, public, private, static..).
 */
public abstract class AbstractBluePrint {
	static int staticVar = 10;
	private int privateVar = 20;
	public int publicVar = 30;

	public AbstractBluePrint() {
	}

	public AbstractBluePrint(int privateVar) {
		this.privateVar = privateVar;
	}
	
	public void setVars(int pri, int pub)
	{
		this.privateVar = pri;
		this.publicVar = pub;
	}
	
	abstract int getPrivate();

	public static void main(String[] args) {
		//Compile time error
		//AbstractBluePrint print = new AbstractBluePrint();
	}
}
