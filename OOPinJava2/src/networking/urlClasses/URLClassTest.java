package networking.urlClasses;

import java.net.URL;

public class URLClassTest {

	public static void main(String[] args) {
		try {
			String mySite = "http://www.msyeed.weebly.com";
			String google = "http://www.google.com";
			String tutorialPoint = "http://www.javatpoint.com/java-tutorial";
			
			URL url = new URL(tutorialPoint);

			System.out.println("Protocol: " + url.getProtocol());
			System.out.println("Host Name: " + url.getHost());
			System.out.println("Port Number: " + url.getPort());
			System.out.println("File Name: " + url.getFile());

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
