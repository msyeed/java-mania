package polymorphism;

public class ICICBank extends Bank{
	public float getInterestRate()
	{
		return (float) 6.5;
	}

	public String toString()
	{
		return ICICBank.class.getName();
	}
}
