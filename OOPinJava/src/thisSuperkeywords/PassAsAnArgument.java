package thisSuperkeywords;


class ReceiveThisReference{
	
	public void getClassReference(SendThisReference obj)
	{
		System.out.println("Reference received: " + obj.toString());
	}
}

class SendThisReference{
	
	public void sendReference()
	{
		System.out.println("Reference sent: " + this);
		
		ReceiveThisReference receiver = new ReceiveThisReference();
		receiver.getClassReference(this);
	}
}

public class PassAsAnArgument {

	public static void main(String[] args) {
		SendThisReference senderObject = new SendThisReference();
		
		senderObject.sendReference();

	}

}
