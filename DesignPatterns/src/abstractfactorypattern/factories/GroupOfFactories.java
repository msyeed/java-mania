package abstractfactorypattern.factories;

import abstractfactorypattern.boxfactoryimp.IShape;
import abstractfactorypattern.colorfactoryimp.IColor;

public abstract class GroupOfFactories {

	//no access modifier here.
	//use with the concrete classes only for those methods that you want to implement and expose for that class.
	abstract IShape getShape (String shape);
	abstract IColor getColor (String color);
}
