package polymorphism;

public class Bank {
	public float getInterestRate()
	{
		return 8;
	}
	
	public String toString()
	{
		return Bank.class.getName();
	}

}
