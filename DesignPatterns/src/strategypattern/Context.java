package strategypattern;

public class Context {
	IStrategy arithmeticStrateg;
	
	public void setArithmeticStrategy(IStrategy arithmeticStrategy)
	{
		this.arithmeticStrateg = arithmeticStrategy;
	}
	
	
	public Integer executeArithmetic(int num1, int num2)
	{
		if (arithmeticStrateg != null)
			return arithmeticStrateg.doArithmetic(num1, num2);
		else
			return null;
	}
}
