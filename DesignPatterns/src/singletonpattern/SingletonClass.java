package singletonpattern;

public class SingletonClass {
	
	//a single static instance of this class object
	private static SingletonClass singletonClass = new SingletonClass();
	
	//constructor is private. So no instantiation is possible
	private SingletonClass(){}
	
	//only static public interface to get the only instance of this class
	public static SingletonClass getInstance()
	{
		return singletonClass;
	}
	
	
	//put your all other stuffs (functionalities here)
	
	//e.g...
	private String msg = "";
	
	public void setMessage(String msg)
	{
		this.msg = msg;
	}
	
	public void showMessage()
	{
		System.out.println("Hello From singleton class!");
		System.out.println("Your message: " + msg);
	}

}
