package innerclass.memberInnerClass;

import innerclass.MemberInnerClass;
import innerclass.MemberInnerClass.PublicInnerClass;

public class InnerMemberClassDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//public inner class can be accessed with the referene of the outer class
		MemberInnerClass.PublicInnerClass publicInnerClass = new MemberInnerClass().new PublicInnerClass();
		//accessing the print method of the innerclass
		publicInnerClass.print();
		
		//private inner class cannot be accessed outside the outerclass even throught the 
		//public method	
		MemberInnerClass outerClassInstance = new MemberInnerClass();
		
		//(ERROR!)get the instance of the private inner class
		//outerClassInstance.getPrivateInnerClassInstance().print();
		
		//outer class excess the private inner class.
		outerClassInstance.printfromPrivateInnerClassInstance();
		
	}

}
