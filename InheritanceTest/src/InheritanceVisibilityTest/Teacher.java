package InheritanceVisibilityTest;

public class Teacher {

	public void printVariables()
	{
		//System.out.println("Student Sub Class: " + " Name is:- " + name);  //cannot access private variables
		System.out.println("Teacher in Package Class: " + " AGE is:- " + new Person("Teacher").age);
		System.out.println("Teacher in PackageTeacher in Package Class: " + " SALARY is:- " + new Person("Teacher").salary);
		System.out.println("Teacher in Package Class: " + " Email is:- " + new Person("Teacher").email);
	}

}
