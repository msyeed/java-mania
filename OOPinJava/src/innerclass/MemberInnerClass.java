package innerclass;

public class MemberInnerClass {
	
	private int x = 30;
	
	
	//create a private inner class
	private class PrivateInnerClass{
		
		public void print()
		{
			System.out.println( toString() +"....Accessing Outer class: " + x);
		}
		
		public String toString()
		{
			return PrivateInnerClass.class.toString();
		}
		
	}
	
	
	
	//create a public inner class
	public class PublicInnerClass{
		public void print()
		{
			System.out.println(toString() +"....Accessing Outer class: " + x);
		}
		
		public String toString()
		{
			return PublicInnerClass.class.toString();
		}
	}
	
	public PrivateInnerClass getPrivateInnerClassInstance()
	{
		return new PrivateInnerClass();
	}
	
	public void printfromPrivateInnerClassInstance()
	{
		new PrivateInnerClass().print();
	}

}
