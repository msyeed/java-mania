package exceptionhandlingWithMethodOverloading;

import java.io.IOException;


/**
* Rule: If the superclass method does not declare an exception, subclass overridden method can declare unchecked exception.
*
*/

class SuperClassTwo{
	
	public void printMSG()
	{
		System.out.println("I am super");
	}
}


public class SuperSubTestTwo extends SuperClassTwo{

	public void printMSG() throws ArithmeticException{
		System.out.println("I am sub.. trying to throw an exception!");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SuperClassTwo p = new SuperSubTestTwo();
		p.printMSG();
	}

}
