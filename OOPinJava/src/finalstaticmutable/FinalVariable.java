package finalstaticmutable;

public class FinalVariable {

	final int instanceFinal;
	static final int classFinal;
	
	
	//immutable / final class variables are initialized within static block
	static{
		classFinal = 20;		
	}
	
	//immutable / final instance variables must be initialized at the time of instantiation
	public FinalVariable(int value)
	{
		this.instanceFinal = value;
		//this.classx = new TestClass();
	}
	
	
	public static void main(String[] args) {
		
		//Error! cannot change an immutable class variable 
		//classFinal = 30;
		
		//immutable instance variables can only be initialized at time of instantiation
		FinalVariable testClass = new FinalVariable(10);
		
		//Error! cannot change an immutable variable value
		//testClass.instanceFinal = 50;

	}

}
