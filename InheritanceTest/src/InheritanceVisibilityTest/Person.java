package InheritanceVisibilityTest;

public class Person {
	
	/**
	 * Only visible within the class
	 * Can be accessed only by public setter and getter methods
	 */
	private String name = "rajit";
	
	/**
	 * Visible to 
	 * (a) Within the class in whcih it is declared.
	 * (b) All subclasses can access it [Within the package].
	 * (c) Any class within the package.
	 */
	protected int age = 10;
	
	/**
	 * Its a "PACKAGE" variable
	 * Visible to 
	 * (a) Within the class in whcih it is declared.
	 * (b) All subclasses can access it [Within the package].
	 * (c) Any class within the package.
	 */
	double salary = 20.57;
	
	/**
	 * Visible to the world
	 */
	public String email = "rajit.cit";
	
	
	public Person(String name)
	{
		this.name = name;
		System.out.println("Person Class: " + this + "   Name:- " +  this.name);
	}
	
	public String getName()
	{
		return name;
	}
}
