package abstractfactorypattern.factories;


import abstractfactorypattern.boxfactoryimp.IShape;
import abstractfactorypattern.colorfactoryimp.Blue;
import abstractfactorypattern.colorfactoryimp.Green;
import abstractfactorypattern.colorfactoryimp.IColor;
import abstractfactorypattern.colorfactoryimp.Red;

public class ColorFactory extends GroupOfFactories{

	//with no access modifier in the method name this will not be visible to classes outside the package
	IShape getShape(String shapeType)
	{
			return null;
	}

	@Override
	public IColor getColor(String color) {
		if(color.equalsIgnoreCase("red"))
		{
			return new Red();
		}
		else if(color.equalsIgnoreCase("green"))
		{
			return new Green();
		}
		else if(color.equalsIgnoreCase("blue"))
		{
			return new Blue();
		}
		else
			return null;
	}
}