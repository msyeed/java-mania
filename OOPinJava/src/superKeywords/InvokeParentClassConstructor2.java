package superKeywords;


class Vehicles {
	Vehicles() {
		System.out.println("Vehicles are created");
	}
}

class Bikes extends Vehicles {
	int speed;

	//super must be the first instruction of the constructor
	Bikes() {
		super();
		this.speed = 20;
		System.out.println("Bikes speed: " + speed);
	}
}

public class InvokeParentClassConstructor2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bikes bike = new Bikes();
	}

}


