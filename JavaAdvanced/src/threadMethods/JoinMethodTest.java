package threadMethods;

public class JoinMethodTest {

	public static class ThreadClass extends Thread {
		public void run() {
			try {
				for (int i = 0; i < 5; i++) {
					//currentThread() method
					System.out.println("Thread " + Thread.currentThread().getName() + " with id " + Thread.currentThread().getId() + "  printing..." + i);
					Thread.sleep(500);
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ThreadClass t0 = new ThreadClass();
		ThreadClass t1 = new ThreadClass();
		ThreadClass t2 = new ThreadClass();
		
		t0.start();
		try {
			System.out.println(t0.getName() +" Join()");
			t0.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		t1.start();
		t2.start();
		
		/*
		 * Thre running thread need to be interrupted (e.g., sleep()) for another thread to join()
		try {
			System.out.println(t0.getName() +" Join()");
			t0.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
	}

}
