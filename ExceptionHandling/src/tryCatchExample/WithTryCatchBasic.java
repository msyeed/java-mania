package tryCatchExample;

public class WithTryCatchBasic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int x = 10;
		int y = 0;
		
		try{
			int z = x / y; //should be divide by 0 exception. It is an unchecked / runtime exception
			System.out.println("Print result: " + z);
		}
		catch(ArithmeticException e)
		{
			System.out.println("Exception Caught! \n" + e.getMessage());
		}
		
		
		//now this code will be executed
		System.out.println("After the exception!");
	}
	
}
