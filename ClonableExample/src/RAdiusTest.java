
public class RAdiusTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double radius = .0009;
		float angleInDegrees = 360;
		double latitute = 60.16109334;
		double longitude = 24.54581765;
		
		double x = (radius * Math.cos(angleInDegrees * Math.PI / 180F)) + latitute;
        double y = (radius * Math.sin(angleInDegrees * Math.PI / 180F)) + longitude;
        
        System.out.println(x +"   " + y);
	}

}
