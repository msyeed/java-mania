package datetime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CustomDateTimeFormatting {

	public static void main(String[] args) {
		
		Date d;
        String stringDate;
        DateFormat format;
        
        try {
        	stringDate = "7-Jun-2013";
			format = new SimpleDateFormat("d-MMM-yyyy", Locale.ENGLISH);
			d = format.parse(stringDate);
			System.out.println("Parshed date is: "+d);
			
			stringDate = "07/06/2013";
			format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			d = format.parse(stringDate);
			System.out.println("Parshed date is: "+d);
			
			stringDate = "Jun 7, 2013";
			format = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);
			d = format.parse(stringDate);
			System.out.println("Parshed date is: "+d);
			
			stringDate = "Friday, Jun 7, 2013 12:10:56 PM";
			format = new SimpleDateFormat("E, MMM dd, yyyy hh:mm:ss a", Locale.ENGLISH);
			d = format.parse(stringDate);
			System.out.println("Parshed date is: "+d);
		} 
		catch (ParseException e) 
		{
			System.out.println("Date not Recognized!");
			e.printStackTrace();
		}  

	}

}
