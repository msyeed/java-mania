package statepattern;

public interface State {
	//action to be performed on state change (TV on or off)
	void doAction(Context context);
}
