package ShallowCopyObjectExample;

public class Employee implements Cloneable{
	 
	//two primitive data type
	private int empoyeeId;
	private String employeeName;
	
	//object type 
	private Department department;
	 
	//constructor
	public Employee(int id, String name, Department dept)
	{
		this.empoyeeId = id;
		this.employeeName = name;
		this.department = dept;
	}
	
	
	//default cloning (no overriding)
	//this will clone only the primitive data types but not the object 
	//(this will be shallow coppied, hence both original and cloned object will have the same reference)
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	     
	    //Accessor/mutators methods will go there
	public int getEmployeeId()
	{
		return empoyeeId;
	}

}
