package customException;

public class CustomExceptionTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			printName("Mou");
		} catch (MyNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	private static void printName(String s) throws MyNameException{
		
		if(!s.equalsIgnoreCase("rajit"))
			throw new MyNameException("This is not my name!");
	}
	
	
	
	public static class MyNameException extends Exception{

		public MyNameException(String s)
		{
			super(s);
		}
	}
}
