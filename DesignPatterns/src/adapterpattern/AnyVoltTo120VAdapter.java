package adapterpattern;

public class AnyVoltTo120VAdapter implements IPowerAdapter{

	@Override
	public int supply120VPower(VoltTypes supplyVoltage) {
		// TODO Auto-generated method stub
		switch(supplyVoltage)
		{
		case OneTwentyVolt:
			return new Produce120V().supply120V();
		case TwoFortyVolt:
			return new Produce240V().supply240V() / 2;
		case EightyVolt:
			return (new Produce80V().supply80V() /2 )*3;
		}
		return 0;
	}

	
}
