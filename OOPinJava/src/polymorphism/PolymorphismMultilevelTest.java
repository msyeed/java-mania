package polymorphism;

public class PolymorphismMultilevelTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		SuperClass superclass = new SuperClass();
		SuperClass subclasslevel1 = new SubclassLevelOne();
		SuperClass subclasslevel2 = new SubclassLevelTwo();
		
		System.out.println("MSG from " + superclass.toString() + " is:- " + superclass.printMsg());
		System.out.println("MSG from " + subclasslevel1.toString() + " is:- " + subclasslevel1.printMsg());
		System.out.println("MSG from " + subclasslevel2.toString() + " is:- " + subclasslevel2.printMsg());
		
	}

}
