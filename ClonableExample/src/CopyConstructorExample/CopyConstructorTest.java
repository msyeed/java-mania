package CopyConstructorExample;

public class CopyConstructorTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		DefinePoint pointOne = new DefinePoint(2,4);
		
		DefinePoint pointCopyofOne = new DefinePoint(pointOne);
		
		System.out.println("Are both same object: " + (pointOne == pointCopyofOne)); //check if they are same object instances
		System.out.println("Do both object refer to same class: " + (pointOne.getClass() == pointCopyofOne.getClass()));
		
		
		//as they are different objects so changing one should not affect the other
		pointOne.setXY(8,  10);
		
		System.out.println("Values in pointOne: " + pointOne.toString());
		System.out.println("Values in pointCopyofOne: " + pointCopyofOne.toString());
		
	}

}
