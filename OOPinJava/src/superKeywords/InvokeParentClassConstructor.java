package superKeywords;

class Vehicle {
	Vehicle() {
		System.out.println("Vehicle is created");
	}
}

class Bike extends Vehicle {
	int speed;

	//compiler adds super() method implicitly to call parent class
	//constructor if not explicitly provided.
	Bike(int speed) {
		this.speed = speed;
		System.out.println(speed);
	}
}

public class InvokeParentClassConstructor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bike bike = new Bike(20);
	}

}
