package abstractfactorypattern;

import abstractfactorypattern.factories.BoxFactory;
import abstractfactorypattern.factories.ColorFactory;
import abstractfactorypattern.factories.GroupOfFactories;

public class FactoryReference {
	
	private static FactoryReference factoryReference = new FactoryReference();
	
	private FactoryReference(){}

	public static FactoryReference getFactoryReference()
	{
		return factoryReference;
	}
	
	public BoxFactory getBoxFactory()
	{
		return new BoxFactory();
	}
	
	public ColorFactory getColorFactory()
	{
		return new ColorFactory();
	}
	
	
	/*
	 * ---------------------------------------------------------
	 * Alternate implementation of returning factory references
	 * ---------------------------------------------------------
	public static GroupOfFactories getFactory(String choice){
		   
	      if(choice.equalsIgnoreCase("BOX")){
	         return new BoxFactory();
	         
	      }else if(choice.equalsIgnoreCase("COLOR")){
	         return new ColorFactory();
	      }
	      
	      return null;
	   }
	   */
}
