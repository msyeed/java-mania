package abstractfactorypattern.factories;

import abstractfactorypattern.boxfactoryimp.Circle;
import abstractfactorypattern.boxfactoryimp.IShape;
import abstractfactorypattern.boxfactoryimp.Rectangle;
import abstractfactorypattern.boxfactoryimp.Square;
import abstractfactorypattern.colorfactoryimp.IColor;

public class BoxFactory extends GroupOfFactories{

	public IShape getShape(String shapeType)
	{
		
		if(shapeType.equalsIgnoreCase("circle"))
		{
			return new Circle();
		}
		else if(shapeType.equalsIgnoreCase("rectangle"))
		{
			return new Rectangle();
		}
		else if(shapeType.equalsIgnoreCase("square"))
		{
			return new Square();
		}
		else
			return null;
	}

	//with no access modifier in the method name this will not be visible to classes outside the package
	@Override
	IColor getColor(String color) {
		// TODO Auto-generated method stub
		return null;
	}

}
