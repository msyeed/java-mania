package misselinious;

import static java.lang.System.*;
import static java.lang.Math.*;


public class StaticImport {

	public static void main(String[] args) {

		out.println("Static Import test. No need to use System class prefix :)");  // Now no need of System.out
		
		double theta = 2.0;
		double r = cos(PI * theta);  // Otherwise it would be    double r = Math.cos(Math.PI * theta);
	}

}
