package superKeywords;

class BasicWelcomeNote{
	
	public void message()
	{
		System.out.print("Welcome ");
	}
}

class GameCourse extends BasicWelcomeNote{
	//overridden method.
	//adding custom message to that of the basic welcome message
	public void message()
	{
		super.message();
		System.out.println("to the world of gamming mobile devices with Rajit!!");
	}
}

class JAVACourse extends BasicWelcomeNote{
	public void message()
	{
		super.message();
		System.out.println("to the world of classical JAVA learning with Rajit!!");
	}
}

public class InvokeParentClassMethod {

	public static void main(String[] args) {
		GameCourse game = new GameCourse();
		game.message();
		
		JAVACourse java = new JAVACourse();
		java.message();

	}

}
