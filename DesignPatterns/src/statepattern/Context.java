package statepattern;

public class Context {
	
	//holds the latest state of the object
	private State state = null;
	
	public void setState(State state)
	{
		this.state = state;
	}
	
	public State getState()
	{
		return state;
	}
	
	
}
