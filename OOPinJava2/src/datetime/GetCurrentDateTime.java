package datetime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class GetCurrentDateTime {

	public static void main(String[] args) {

		// Option 1: java.util.Date
		java.util.Date date = new java.util.Date();
		System.out.println("java.util.Date:- " + date);

		// Option 2: System static class
		long timeInMills = System.currentTimeMillis();
		java.util.Date date2 = new java.util.Date(timeInMills);
		System.out.println("System class:- " + date2);

		// Option 3: (RECOMMENDED OPTION) java.util.Calendar
		java.util.Date date3 = java.util.Calendar.getInstance().getTime();
		System.out.println("java.util.Calendar:- " + date3);
		
		// Option 4: java.sql.Date 
		// this class only prints date but not time. Generally used in relation to DB insert for a DATE column.
		long millis = System.currentTimeMillis();  
		java.sql.Date date4 = new java.sql.Date(millis);  
		System.out.println("java.sql.Date:- " + date4); 
		
	}

}
