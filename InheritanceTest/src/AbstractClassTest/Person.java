package AbstractClassTest;

/**
 * this class should have
 * 
 * (a) common instane variables: name, salary
 * (b) common method to set and get name: e.g., constructors and setter getter
 * (c) different implementation for salary.
 * 
 * @author admin
 *
 */
public abstract class Person {
	private String name;
		
	public Person(String name)
	{
		this.name = name;
		System.out.println("Person Class:  Person Name:- " +  this.name);
	}
	
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	
	public abstract void setSalary(int salary);
	public abstract int getSalary();
}
