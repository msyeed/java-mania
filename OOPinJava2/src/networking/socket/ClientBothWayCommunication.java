package networking.socket;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientBothWayCommunication {

	public static void main(String[] args) throws Exception {
		//create a socket to accept / send data through the port.
		Socket s = new Socket("localhost", 3333);
		
		//input stream to accept data from client
		DataInputStream din = new DataInputStream(s.getInputStream());
		//output stream to send data to client
		DataOutputStream dout = new DataOutputStream(s.getOutputStream());
		
		//a buffer reader to get data from command line
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str = "", str2 = "";
		
		//until you explicitly write stop to stop the client
		while (!str.equals("stop")) {
			
			//get and send the data to the server
			str = br.readLine();
			dout.writeUTF(str);
			dout.flush();
			
			//receive data from server and print it.
			str2 = din.readUTF();
			System.out.println("Server says: " + str2);
		}

		dout.close();
		s.close();

	}

}
