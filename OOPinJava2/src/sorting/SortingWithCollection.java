package sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;



public class SortingWithCollection {

	public static void main(String[] args) {
		//list holding String objects
		ArrayList<String> names = new ArrayList<String>();
		names.add("rajit");
		names.add("mou");
		names.add("suha");
		//list holding wrapper Class objects
		LinkedList<Integer> numbers = new LinkedList<Integer>();
		numbers.add(Integer.valueOf(100));
		numbers.add(Integer.valueOf(50));
		numbers.add(Integer.valueOf(1));
		
		//now sort the both and print
		Collections.sort(names);
		Collections.sort(numbers);
		
		Iterator<String> it = names.iterator();
		while(it.hasNext())
			System.out.println("name: " + it.next());
		
		Iterator<Integer> it_int = numbers.iterator();
		while(it_int.hasNext())
			System.out.println("name: " + it_int.next());
	}

}
