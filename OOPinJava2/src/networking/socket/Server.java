package networking.socket;

import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	public static void main(String[] args) {
		try {
			//create a server socket with a port number
			ServerSocket ss = new ServerSocket(6666);
			//establish a connection so that the servcer could accept / communicate with client
			//through this port
			Socket s = ss.accept();
			
			//receive the data stream send by client through this port
			DataInputStream dis = new DataInputStream(s.getInputStream());
			String str = (String) dis.readUTF();
			//print the data in the console
			System.out.println("message= " + str);
			//close the connection.
			ss.close();
		} 
		catch (Exception e) 
		{
			System.out.println(e);
		}
	}
}
