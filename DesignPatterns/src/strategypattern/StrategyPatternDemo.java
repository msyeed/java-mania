package strategypattern;

public class StrategyPatternDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Context context = new Context();
		
		context.setArithmeticStrategy(new AddArithmeticStrategy());
		int x = context.executeArithmetic(3, 4);
		System.out.println(x);
		
		context.setArithmeticStrategy(new SubtractArithmeticStrategy());
		x = context.executeArithmetic(3, 4);
		System.out.println(x);
		
		context.setArithmeticStrategy(new DivisionArithmeticStrategy());
		x = context.executeArithmetic(4, 3);
		System.out.println(x);
		
		context.setArithmeticStrategy(new DivisionArithmeticStrategy());
		x = context.executeArithmetic(3, 0);
		System.out.println(x);
	}

}
