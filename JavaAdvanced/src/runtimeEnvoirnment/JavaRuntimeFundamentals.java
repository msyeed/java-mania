package runtimeEnvoirnment;

import java.io.IOException;

public class JavaRuntimeFundamentals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// returns the runtime instance for the current application
		Runtime runTime = Runtime.getRuntime();

		try {
			runTime.exec("notepad"); // starts the notepad application.
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// returns the number of available processor in this machine
		System.out.println("Available processors: "
				+ runTime.availableProcessors());

		// free memory available for JVM
		System.out.println("Free memory: " + runTime.freeMemory());

		// total memory available for JVM
		System.out.println("Total memory: " + runTime.totalMemory());

		// lets consume some memory
		for (int i = 0; i < 10000; i++) {
			new MemoryTest();
		}
		System.out.println("After creating 10000 instance, Free Memory: "
				+ runTime.freeMemory());

		System.gc(); // garbadge collect all the unreferenced objects

		System.out.println("After gc(), Free Memory: " + runTime.freeMemory());

		runTime.exit(0); // terminates the current virtual machine.
		/*
		 * 3) public void addShutdownHook(Thread hook) registers new hook
		 * thread.
		 */
	}

}

class MemoryTest {
}
