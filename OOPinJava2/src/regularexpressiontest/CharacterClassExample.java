package regularexpressiontest;

import java.util.regex.Pattern;

public class CharacterClassExample {

	public static void main(String args[]){  
		
		//the character class pattern matches for only one character in the pattern.
		//so the given string to be matched should consists of only one character which is within the 
		//given pattern.
		
		System.out.println(Pattern.matches("[amn]", "amn"));//false (more than one character)  
		System.out.println(Pattern.matches("[amn]", "a"));//true (among a or m or n)  
		System.out.println(Pattern.matches("[amn]", "n"));//true (among a or m or n)
		System.out.println(Pattern.matches("[amn]", "mn"));//false (more than one character)
		
		System.out.println(Pattern.matches("[^amn]", "b"));//true (not among a or m or n)
		System.out.println(Pattern.matches("[^amn]", "n"));//false (among a or m or n)
		System.out.println(Pattern.matches("[^amn]", "bc"));//false (more than one character)
		
		System.out.println(Pattern.matches("[a-zA-Z]", "B"));//true (among a - z or A-Z)
		System.out.println(Pattern.matches("[a-zA-Z]", "BER"));//false (more than one character)
		
		System.out.println(Pattern.matches("[a-d[w-z]]", "e"));//false (not within the range)
		System.out.println(Pattern.matches("[a-d[w-z]]", "abc"));//false (more than one character)
		System.out.println(Pattern.matches("[a-d[w-z]]", "a"));//true (one character within the range)
		
		System.out.println(Pattern.matches("[a-d&&[w-z]]", "a"));//false (no letter falls within the intersection of the two ranges)
		System.out.println(Pattern.matches("[a-d&&[a-z]]", "a"));//true ([a,b,c,d] are within the intersection of the two)
		System.out.println(Pattern.matches("[a-d&&[a-z]]", "e"));//false (e is not within the intersection range, [a,b,c,d])
		
		System.out.println(Pattern.matches("[a-z&&[^w-z]]", "w"));//false (the range is a-z except w-z)
		System.out.println(Pattern.matches("[a-z&&[^d-g]]", "g"));//false (the range is a-z except d-g)
		System.out.println(Pattern.matches("[a-z&&[^d-g]]", "a"));//true (within range)
		}
}
