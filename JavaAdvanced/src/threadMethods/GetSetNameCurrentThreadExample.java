package threadMethods;


public class GetSetNameCurrentThreadExample {

	public static class ThreadClass extends Thread {
		public void run() {
			try {
				for (int i = 0; i < 5; i++) {
					//currentThread() method
					System.out.println("Thread " + Thread.currentThread().getName() + " with id " + Thread.currentThread().getId() + "  printing..." + i);
					Thread.sleep(500);
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//creating two threads.
		ThreadClass threadOne = new ThreadClass();
		
		ThreadClass threadTwo = new ThreadClass();
		
		//getName(), getId(), getPriority() method
		System.out.println("Thread one name, id and priority: " + threadOne.getName() + "::" + threadOne.getId() + "-" + threadOne.getPriority());
		System.out.println("Thread one name, id and priority: " + threadTwo.getName() + "::" + threadTwo.getId() + "-" + threadTwo.getPriority());
		threadOne.start();
		threadTwo.start();
		
		//lets change the name of Thread-0 after one second!
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		threadOne.setName("Rajit Thread");  //setName() method
	}
}
