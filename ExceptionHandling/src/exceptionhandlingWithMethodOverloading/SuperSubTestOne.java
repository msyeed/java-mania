package exceptionhandlingWithMethodOverloading;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Rule: If the superclass method does not declare an exception, subclass overridden method cannot declare the checked exception.
 *
 */

class SuperClass{
	
	public void printMSG()
	{
		System.out.println("I am super");
	}
}


public class SuperSubTestOne extends SuperClass{

	
	public void printMSG(){
		System.out.println("I am sub.. trying to throw an exception!");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SuperClass p = new SuperSubTestOne();
		p.printMSG();
	}

}
