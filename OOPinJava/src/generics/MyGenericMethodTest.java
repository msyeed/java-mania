package generics;

public class MyGenericMethodTest {
	
	
	public static <E> void printElements(E[] elements)
	{
		for(E element : elements)
		{
			System.out.print(element + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		
		/**
		 * NOTE!! Generics works for object type data not on primitive types!!
		 */
		Integer [] integerElements= {1,2,5,4,6,7};
		
		String [] stringElements = {"rajit", "is", "a", "cute", "boy"};
		
		Character [] charElements = {'R','a','j','i','t'};
		
		printElements(integerElements);
		printElements(stringElements);
		printElements(charElements);
	}

}
