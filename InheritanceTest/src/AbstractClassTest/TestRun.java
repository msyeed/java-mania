package AbstractClassTest;

public class TestRun {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Person s = new Student ("Rajit");
		
		Person [] p = new Person [4];
		
		//p[0] = new Person("Person"); //abstract class can not be instantiated
		
		p[1] = new Student("Student");
		p[2] = new Teacher("Teacher");
		p[3] = new Student();
		
		p[1].setSalary(20);
		p[2].setSalary(30);
		
		
		for(int i = 1; i < p.length ; i++)
		{
			System.out.println("Printing p[" + i +"] salary: " + p[i].getSalary());
		}
	}

}
