package interfaceTest;

interface Print{
	void print();
}

interface Show{
	void print();
	void show();
}

public class MultipleInheritanceWithSameMethod implements Print, Show{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

}
