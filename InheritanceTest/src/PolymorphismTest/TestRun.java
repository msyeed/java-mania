package PolymorphismTest;

public class TestRun {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Person s = new Student ("Rajit");
		
		Person [] p = new Person [4];
		
		p[0] = new Person("Person");
		p[1] = new Student("Student", 1234);
		p[2] = new Teacher("Teacher", 2090);
		p[3] = new Student();
		
				
		//this calls appropriate class reference toString method due to polymorphism
		//as we overwridden the toString method for each subclass, so the class reference will identify
		//correct toString method to call at runtime.
		for(int i = 0; i < p.length ; i++)
			System.out.println("Printing p[" + i +"]" + p[i]);
		
		
		//!!! PROBLEM
		//This will create a compile time error.
		//As at compile time the compiler reference to the person object which does not have the 
		//studientId() method. and also this is not a overwridden method as toString().
		Person x = new Student("Test Student", 2345);
		
		//its a compile type error due to wrong reference [x refere to a person object whcih does not have a getStudentId() method]
		//x.getStudentId();
		
		
		//:) SOLUTION
		//use casting for this cases
		
		if(x instanceof Student){
			int id = ((Student) x).getStudentId();
			System.out.println("Student id is: " + id);
		}
			
	}

}
