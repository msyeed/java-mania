package strategypattern;

public class SubtractArithmeticStrategy implements IStrategy{

	@Override
	public Integer doArithmetic(Integer num1, Integer num2) {
		// TODO Auto-generated method stub				
		return (num1 >= num2)?  (num1 - num2) : (num2 - num1);
	}

}
