package garbadgeCollection;

public class GarbadgeCollectionExampleOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ObjectToBeGarbadged obj1 = new ObjectToBeGarbadged("Object 1");
		ObjectToBeGarbadged obj2 = new ObjectToBeGarbadged("Object 2");
		
		obj1 = obj2;  // now obj1 reference becomes unreferenced thus to be garbadge collected!
		
		
		System.gc();  // calling the garbadge collection explicitly
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//
		// what will happen here?
		//
		obj1 = null;
		System.gc();  // calling the garbadge collection explicitly
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//
		//What will happen here?
		//
		obj2 = null;
		System.gc();  // calling the garbadge collection explicitly
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

class ObjectToBeGarbadged{
	
	private String name;
	
	public ObjectToBeGarbadged(String name)
	{
		this.name = name;
	}
	
	protected void finalize()
	{
		System.out.println(name + " is finalizing!!");
	}
	
}