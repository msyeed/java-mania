package TestPackage;

import InheritanceVisibilityTest.Person;

public class TestInheritanceVisibility extends Person{

	public TestInheritanceVisibility(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println("Student Sub Class: " + " Name is:- " + name);  //cannot access private variables
		
		//only within package..
		//System.out.println("TestInheritanceVisibility outside Package Class: " + " AGE is:- " + new Person("Teacher").age);
		//System.out.println("TestInheritanceVisibility outside PackageTeacher in Package Class: " + " SALARY is:- " + new Person("Teacher").salary);
		
		System.out.println("TestInheritanceVisibility outside Package Class: " + " Email is:- " + new Person("Teacher").email);
	}

}
