package CopyConstructorExample;

public class DefinePoint {

	private int x;
	private int y;
	
	public DefinePoint(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	//the copy constructor
	public DefinePoint(DefinePoint anotherPoint)
	{
		this.x = anotherPoint.x;
		this.y = anotherPoint.y;
	}
	
	public void setXY(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public String toString()
	{
		return "(" + x + ", " + y + ")";
	}
}
