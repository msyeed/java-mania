package polymorphism;

public class PolymorphismTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank sbi = new SBIBank();
		Bank icic = new ICICBank();
		Bank axis = new AXISBank();
		
		//all the overridden method call are resolved at runtime
		//at compile time each of the bank object refers only to the base / super class bank!
		System.out.println("Interest in bank " + sbi.toString() + " is:- " + sbi.getInterestRate());
		System.out.println("Interest in bank " + icic.toString() + " is:- " + icic.getInterestRate());
		System.out.println("Interest in bank " + axis.toString() + " is:- " + axis.getInterestRate());
	}

}
