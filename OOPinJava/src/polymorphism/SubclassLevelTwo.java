package polymorphism;

public class SubclassLevelTwo extends SubclassLevelOne{
	
	public String printMsg()
	{
		return "I am sub class level 2:)";
	}
	
	public String toString()
	{
		return SubclassLevelTwo.class.getName();
	}

}
