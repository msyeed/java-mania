package networking.socket;

import java.io.DataOutputStream;
import java.net.Socket;

public class Client {
	public static void main(String[] args) {
		try 
		{
			//create a socket for connecting to the server on a known port
			Socket s = new Socket("localhost", 6666);
			
			//create a output stream
			DataOutputStream dout = new DataOutputStream(s.getOutputStream());
			//write data to the stream and send it
			dout.writeUTF("Hello Server");
			dout.flush();
			//close the stream while done and close the socket.
			dout.close();
			s.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
