package facadepattern;

public class FacadeDesingPatternDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ShapeBuilder shapeBuilder = new ShapeBuilder();
		
		shapeBuilder.drawCirlce();
		shapeBuilder.drawRectangle();
		shapeBuilder.drawSquare();
	}

}
