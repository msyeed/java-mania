package observerpattern;

public abstract class Observer {

	protected Subject aSubject;
	public abstract void getUpdateOnSubject();
}
