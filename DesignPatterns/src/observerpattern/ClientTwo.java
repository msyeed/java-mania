package observerpattern;

public class ClientTwo extends Observer{

	public ClientTwo(Subject subject)
	{
		this.aSubject = subject;
		aSubject.addObserver(this);
	}
	
	public void getUpdateOnSubject(){
		System.out.println("Client Two: gets the state update. The state is:- " + aSubject.getState());
	}
}
