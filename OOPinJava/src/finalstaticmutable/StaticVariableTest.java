package finalstaticmutable;

public class StaticVariableTest {

	public static String name = "Rajit";
	
	private static String age = "35";
	
	//you need to define setters and getters for private static variable
	public static String getAge() {
		
		return age;
	}
	
	public static void setAge(String age) {
		StaticVariableTest.age = age;
	}
	
	

}
