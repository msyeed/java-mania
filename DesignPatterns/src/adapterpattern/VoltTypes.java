package adapterpattern;

public enum VoltTypes {
	EightyVolt,
	OneTwentyVolt,
	TwoFortyVolt;
}
