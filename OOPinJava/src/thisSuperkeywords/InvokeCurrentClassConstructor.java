package thisSuperkeywords;


class Student{
	private int id;
	private String name;
	private int age;
	
	public Student()
	{
		System.out.println("Default Constructor. Can be called with this keyword");
	}
	
	public Student(int id, String name)
	{
		this (); //calling the default constructor. 
		         //default constructor can be called from a constructor only
		this.id = id;
		this.name = name;
	}
	
	public Student(int id, String name, int age)
	{
		this (id, name); //calling the other two constructors.
		this.age =  age;
	}
	
	public void display()
	{
		System.out.println("id: " + id);
		System.out.println("name: " + name);
		System.out.println("age: " + age);
	}
}

public class InvokeCurrentClassConstructor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Student student = new Student(1, "Rajit");
		student.display();
		
		Student student2 = new Student(2, "Syeed", 31);
		student2.display();
	}

}
