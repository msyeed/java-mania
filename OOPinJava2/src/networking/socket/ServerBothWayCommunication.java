package networking.socket;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerBothWayCommunication {

	public static void main(String[] args) {
		try {
			//create a server socket with a port number
			ServerSocket ss = new ServerSocket(3333);
			//create a socket to accept data through the port.
			Socket s = ss.accept();
			//input stream to accept data from client
			DataInputStream din = new DataInputStream(s.getInputStream());
			//output stream to send data to client
			DataOutputStream dout = new DataOutputStream(s.getOutputStream());
			
			//a buffer reader to get data from command line
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));

			String str = "", str2 = "";
			
			//until you explicitly write stop to stop the server
			while (!str.equals("stop")) 
			{
				//read and print what client says
				str = din.readUTF();
				System.out.println("client says: " + str);
				
				//get data to reply to the client
				str2 = br.readLine();
				dout.writeUTF(str2);
				dout.flush();
			}
			din.close();
			s.close();
			ss.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
