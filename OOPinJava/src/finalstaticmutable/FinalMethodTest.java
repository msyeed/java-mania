package finalstaticmutable;

public class FinalMethodTest extends FinalMethod{

	//Error! final method cannot be overridden by inherited class
	public final void testFinalMethod()
	{
		System.out.println("I am trying to extend the inherited final method!!");
	}
	
	public static void main(String[] args) {
		// your code goes here

	}

}
