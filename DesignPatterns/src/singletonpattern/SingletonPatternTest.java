package singletonpattern;

public class SingletonPatternTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SingletonClass singleton1 = SingletonClass.getInstance();
		//SingletonClass singleton2 = SingletonClass.getInstance();

		singleton1.setMessage("I am calling from singleton1");
		singleton1.showMessage();
		//singleton2.showMessage();
		
		//singleton2.setMessage("I am calling from singleton2");
		//singleton1.showMessage();
		//singleton2.showMessage();

	}

}
