package iteratorpattern;

public class NameRepository implements Container {

	// This is the repository on whcih the iterator will be operating.
	private String names[] = { "Rajit", "Mou", "Suha", "Samah" };

	@Override
	public Iterator getIterator() {
		// TODO Auto-generated method stub
		return new nameIterator();
	}

	/**
	 * Implementation of the iterator for the names repository
	 * this allows only read operation but restricts insertion, deletion or modification.
	 * @author admin
	 *
	 */
	private class nameIterator implements Iterator {

		private int currentIndex = 0;

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return (currentIndex < names.length) ? true : false;
		}

		@Override
		public boolean isEmpty() {
			// TODO Auto-generated method stub
			return (names.length == 0) ? true : false;
		}

		@Override
		public Object next() {
			// TODO Auto-generated method stub
			if (this.hasNext())
				return names[currentIndex++];
			else
				return null;
		}

	}

}
