package factorypattern;

public class ClientForShape {
	public static void main(String[] args) {
		
		ShapeFactory shapeFactory = new ShapeFactory();
		
		Circle circle = (Circle)shapeFactory.getShape("circle");
		Rectangle rectangle = (Rectangle) shapeFactory.getShape("rectangle");
		Square square = (Square) shapeFactory.getShape("square");
		
		circle.draw();
		rectangle.draw();
		square.draw();
		
		IShape aShape = shapeFactory.getShape("circle");
		aShape.draw();
		
		aShape = shapeFactory.getShape("rectangle");
		aShape.draw();
		
		aShape = shapeFactory.getShape("square");
		aShape.draw();
	}

}
