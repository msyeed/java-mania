package iteratorpattern;

public class IteratorPatternDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		NameRepository nameRepo = new NameRepository();
		
		Iterator iter = nameRepo.getIterator(); 
		
		while(iter.hasNext())
			System.out.println(iter.next());
	}

}
