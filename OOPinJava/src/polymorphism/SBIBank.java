package polymorphism;

public class SBIBank extends Bank{
	public float getInterestRate()
	{
		return 7;
	}
	
	public String toString()
	{
		return SBIBank.class.getName();
	}

}
