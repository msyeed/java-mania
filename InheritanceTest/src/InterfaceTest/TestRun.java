package InterfaceTest;

public class TestRun {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//here person is a interface
		Person [] p = new Person [2];
		
		
		//both student and teacher implements person interface,
		//thus they are of person interface type.
		p[0] = new Student();
		p[1] = new Teacher();
		
		p[0].setName("Mou");
		p[1].setName("Rajit");
		
		p[0].setSalary(20);
		p[1].setSalary(30);
		
		
		for(int i = 0; i < p.length ; i++)
		{
			System.out.println("Printing p[" + i +"] salary: " + p[i].getSalary() + "   name: " + p[i].getName());
		}
	}

}
