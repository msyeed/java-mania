package interfaceTest;



interface Test{}

interface ScienceTest extends Test{
	void startScienceTest();
}

interface ZoolozyTest extends Test{
	void startZoolozyTest();
}

public class InterfaceInheritanceTest2 implements ScienceTest, ZoolozyTest{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Test scienceTest = new InterfaceInheritanceTest2();
		Test zoolozyTest = new InterfaceInheritanceTest2();
		
	}

	@Override
	public void startZoolozyTest() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startScienceTest() {
		// TODO Auto-generated method stub
		
	}

}
