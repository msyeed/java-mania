package regularexpressiontest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchTelephoneNumberEmail {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
				
		//match email (a valid gmail address)
		String emailPattern = "[a-z]{1}[a-z0-9]*@[gmail]{5}\\.[com]{3}";
		
		System.out.println(Pattern.matches(emailPattern, "rajit984401@gmail.com"));
		
		//match phone number (finnish phone number in ISD format)
		String phonenumberPattern = "[358]{3}[1-9]{2}[0-9]{7}";
		
		System.out.println(Pattern.matches(phonenumberPattern, "358458791110"));
	}

}
