package InheritanceVisibilityTest;

public class Student extends Person{

	public Student(String name) {
		super(name);
		// TODO Auto-generated constructor stub
		//System.out.println("Class: " + this + "   Name:- " +  super.getName()); // same output as bellow
		//System.out.println("Student Class: " + this + "   Name:- " +  this.getName());
	}
	
	
	public void printVariables()
	{
		//System.out.println("Student Sub Class: " + " Name is:- " + name);  //cannot access private variables
		System.out.println("Student Sub Class: " + " AGE is:- " + age);
		System.out.println("Student Sub Class: " + " SALARY is:- " + salary);
		System.out.println("Student Sub Class: " + " Email is:- " + email);
	}

}
