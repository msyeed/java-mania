package strategypattern;

public interface IStrategy {
	Integer doArithmetic(Integer num1, Integer num2);
}
