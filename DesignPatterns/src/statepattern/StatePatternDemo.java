package statepattern;

public class StatePatternDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//get the context to have the latest state
		Context context = new Context();
		
		//tv is truned on.
		TVOnState turnTvOn = new TVOnState();
		//if tv is off
		if(context.getState() == null || (context.getState() instanceof TVOffState))
			turnTvOn.doAction(context);
		
		//tv is turned off.
		TVOffState turnTvOff = new TVOffState();
		//if tv is on
		if(context.getState() instanceof TVOnState)
			turnTvOff.doAction(context);
		
	}

}
