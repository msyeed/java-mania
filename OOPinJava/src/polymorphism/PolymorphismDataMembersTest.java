package polymorphism;

public class PolymorphismDataMembersTest {

	public static class SuperClass{
		private int x = 100;
		
		public String toString()
		{
			return Integer.toString(x);
		}
	}
	
	public static class SubClass extends SuperClass{
		private int x = 200;
		
		public String toString()
		{
			return Integer.toString(x);
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SuperClass superClass = new SuperClass();
		
		SuperClass subClass = new SubClass();
		
		System.out.println(superClass.toString());
		
		System.out.println(subClass.toString());
	}

}
