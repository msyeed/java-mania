package adapterpattern;

public class Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// say MAC needs 120 v and original supply is 240V.
		System.out
				.println("MAC gets power from the adapter (Original supply voltage is 240V): "
						+ new AnyVoltTo120VAdapter()
								.supply120VPower(VoltTypes.TwoFortyVolt));

		// say MAC needs 120 v and original supply is 80V.
		System.out
				.println("MAC gets power from the adapter (Original supply voltage is 80V): "
						+ new AnyVoltTo120VAdapter()
								.supply120VPower(VoltTypes.EightyVolt));

	}

}
