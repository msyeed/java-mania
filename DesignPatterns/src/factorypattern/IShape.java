package factorypattern;

//this interface will be implemented by the concrete classes having common behavior.
public interface IShape {
	public void draw();

}
