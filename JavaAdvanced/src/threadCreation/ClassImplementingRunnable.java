package threadCreation;

public class ClassImplementingRunnable implements Runnable{

	public void run()
	{
	/*	
		while(true)
		{
			System.out.println("Runnable thread is running!!");
		}
		*/
		while (!Thread.currentThread().isInterrupted()) {
	          // continue processing
			System.out.println("Runnable thread is running!!");
	      }
		
		System.out.println("Thread terminating!!");
		
	}
}
