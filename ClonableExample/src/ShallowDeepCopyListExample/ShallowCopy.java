package ShallowDeepCopyListExample;

import java.util.ArrayList;
import java.util.List;

public class ShallowCopy {

	private static List fromList;
	private static List toList;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		fromList = new ArrayList<Integer>();
		fromList.add(1);
		
		System.out.println("from list size: " + fromList.size());
		
		//shallow copy. Copies only the reference. Therefore both refers to the same physical instance of a list.
		toList = fromList;
		
		toList.add(2);
		
		System.out.println("to list size: " + toList.size());
		System.out.println("from list size (after adding in to list): " + fromList.size());
	}

}
